//
//  ShopViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/4/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PageMenu

class ShopViewController: ASViewController{
    
    var pageMenu: CAPSPageMenu?
    var controllerArray : [ShopPlaceholderViewController] = []
    
    init() {
        let placeWoman = ShopPlaceholderViewController()
        placeWoman.title = "NỮ"
        controllerArray.append(placeWoman)
        
        let manCtr = ShopPlaceholderViewController()
        manCtr.title = "NAM"
        controllerArray.append(manCtr)
        
        let pageMenuOption :[CAPSPageMenuOption] = [
            .CenterMenuItems(true),
            .MenuItemSeparatorWidth(0),
            .EnableHorizontalBounce(false),
            .MenuHeight(42.0),
            .MenuMargin(0),
            .SelectionIndicatorHeight(3.0),
            .MenuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            .BottomMenuHairlineColor(UIColor(red: 12/255.0, green: 12/255.0, blue: 12/255.0, alpha: 1.0)),
            .SelectionIndicatorColor(UIColor(red: 12/255.0, green: 12/255.0, blue: 12/255.0, alpha: 1.0)),
            .ViewBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)),
            .ScrollMenuBackgroundColor(UIColor(red: 255/255, green:255/255, blue: 255/255, alpha: 1)),
            .UnselectedMenuItemLabelColor(UIColor(red: 140 / 255.0, green: 140 / 255.0, blue: 140 / 255.0, alpha: 1.0)),
            .SelectedMenuItemLabelColor(UIColor(red: 12/255.0, green: 12/255.0, blue: 12/255.0, alpha: 1.0))
            ]

        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0, 0, screenWidth, screenHeight), pageMenuOptions: pageMenuOption)
        let node = ASCellNode()
        super.init(node: node)
        
        self.view.addSubview(pageMenu!.view)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        for ctr in controllerArray {
            ctr.parentNavigationController = navigationController
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
