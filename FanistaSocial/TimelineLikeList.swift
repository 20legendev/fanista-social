//
//  TimelineLikeList.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/28/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit


class TimelineLikeList: ASCellNode, ASCollectionDelegate, ASCollectionDataSource{
    
    var collectionNode: ASCollectionNode?
    var arr = [
        "https://scontent-hkg3-1.cdninstagram.com/t51.2885-19/s150x150/12797761_665672120241001_178703132_a.jpg",
        "https://scontent-hkg3-1.cdninstagram.com/t51.2885-19/s150x150/916548_1697153983881506_748551837_a.jpg",
        "https://scontent-hkg3-1.cdninstagram.com/t51.2885-19/s150x150/12277458_938407566253860_1175573535_a.jpg",
        "https://scontent-hkg3-1.cdninstagram.com/l/t51.2885-19/s150x150/12930896_861467540646578_1918608213_a.jpg",
        "https://igcdn-photos-b-a.akamaihd.net/hphotos-ak-xap1/t51.2885-19/s150x150/13266904_1017545664987913_1338367586_a.jpg",
        "https://scontent-hkg3-1.cdninstagram.com/t51.2885-19/s150x150/12346208_453996938139261_437370138_a.jpg",
        "https://scontent-hkg3-1.cdninstagram.com/t51.2885-19/s150x150/13108875_108443536230621_1905725821_a.jpg"
    ]

    override init(){
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing  = 9
        flowLayout.minimumLineSpacing       = 1
        let node = ASCollectionNode(collectionViewLayout: flowLayout)
        super.init()
        node.delegate = self
        node.dataSource = self
        addSubnode(node)
        self.collectionNode = node
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        if indexPath.row == arr.count - 1{
            return CommentCountNode(counter: String(arr.count))
        }else{
            return AvatarNode(imageURL: arr[indexPath.row % 8]);
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        collectionNode!.preferredFrameSize = CGSize(width: self.bounds.size.width, height: 28)
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 10, 0, 10),
            child: collectionNode!)
    }
    
}