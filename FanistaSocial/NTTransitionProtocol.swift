//
//  NTTransitionProtocol.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit

@objc protocol PopViewProtocol{
    func getCollectionNode() -> ASCollectionNode!
}

@objc protocol NTTransitionProtocol{
    func transitionCollectionView() -> ASCollectionNode!
}

@objc protocol NTTansitionWaterfallGridViewProtocol{
    func snapShotForTransition() -> UIView!
}

@objc protocol NTWaterFallViewControllerProtocol : NTTransitionProtocol{
    func viewWillAppearWithPageIndex(pageIndex : NSInteger)
}

@objc protocol NTHorizontalPageViewControllerProtocol : NTTransitionProtocol{
    func pageViewCellScrollViewContentOffset() -> CGPoint
}