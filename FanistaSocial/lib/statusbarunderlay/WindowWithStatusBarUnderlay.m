//
//  WindowWithStatusBarUnderlay.m
//  Sample
//
//  Created by Hannah Troisi on 4/10/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "WindowWithStatusBarUnderlay.h"
// #import "Utilities.h"

@implementation WindowWithStatusBarUnderlay
{
  UIView *_statusBarOpaqueUnderlayView;
}

-(instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    _statusBarOpaqueUnderlayView                 = [[UIView alloc] init];
      _statusBarOpaqueUnderlayView.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                                     green:255.0f/255.0f
                                                                      blue:255.0f/255.0f
                                                                     alpha:1.0f];
    [self addSubview:_statusBarOpaqueUnderlayView];
  }
  return self;
}

-(void)layoutSubviews
{
  [super layoutSubviews];
  
  [self bringSubviewToFront:_statusBarOpaqueUnderlayView];
  
  CGRect statusBarFrame              = CGRectZero;
  statusBarFrame.size.width          = [[UIScreen mainScreen] bounds].size.width;
  statusBarFrame.size.height         = [[UIApplication sharedApplication] statusBarFrame].size.height;
  _statusBarOpaqueUnderlayView.frame = statusBarFrame;
}

@end
