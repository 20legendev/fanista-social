//
//  ProductDetailImageSliderCell.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/22/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import AsyncDisplayKit

class ProductDetailImageSliderCell: ASCellNode{
    var imageModel: ImageModel
    var imageNode: ASNetworkImageNode
    init(imgUrl: ImageModel){
        imageModel = imgUrl
        imageNode = ASNetworkImageNode()
        imageNode.URL = NSURL(string: imageModel.url)
//        imageNode.preferredFrameSize = CGSize(width: screenWidth, height: screenHeight - navigationHeaderAndStatusbarHeight - tabbarHeight)
//        imageNode.preferredFrameSize = CGSize(width: screenWidth, height: screenWidth * (imageModel.height/imageModel.width))
        super.init()
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let spec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Center, alignItems: .Stretch, children: [ASRatioLayoutSpec(ratio: CGFloat(imageModel.height) / CGFloat(imageModel.width), child: imageNode)])
//        let spec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Center, alignItems: .Stretch, children: [imageNode])
        
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: spec)
    }
}