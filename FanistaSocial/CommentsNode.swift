//
//  CommentNode.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 8/23/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

let INTER_COMMENT_SPACING: CGFloat = 5.0
let NUM_COMMENTS_TO_SHOW: Int  = 3

class CommentsNode: ASCellNode {
    private var commentFeed: CommentFeedModel?
    private var commentNodes: [ASTextNode]
    
    init(row: Int) {
        commentNodes = [ASTextNode]()
        super.init()
    }
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let verticalStack = ASStackLayoutSpec.verticalStackLayoutSpec()
        verticalStack.spacing = INTER_COMMENT_SPACING
        verticalStack.setChildren(commentNodes)
        return verticalStack
    }
    
    func updateWithCommentFeedModel(feed: CommentFeedModel){
        commentFeed = feed
        self.removeCommentLabels()
        let numOfRow = self.createCommentLabels()
        
        let addViewAllCommentsLabel = feed.numberOfCommentsForPhotoExceedsInteger(NUM_COMMENTS_TO_SHOW)
        var labelsIndex = 0
        
        let commentLabelString = commentFeed!.comments[labelsIndex].commentAttributedString()
        commentNodes[labelsIndex].attributedString = commentLabelString;
        labelsIndex += 1

        if addViewAllCommentsLabel {
            let commentLabelString = commentFeed!.viewAllCommentsAttributedString()
            commentNodes[labelsIndex].attributedString = commentLabelString;
            labelsIndex += 1
        }
        for feedIndex in 1...numOfRow - 2{
            let commentLabelString = commentFeed!.comments[feedIndex].commentAttributedString()
            commentNodes[labelsIndex].attributedString = commentLabelString;
            labelsIndex += 1
        }
        self.setNeedsLayout()
    }
    
    func removeCommentLabels(){
        for commentLabel in commentNodes {
            commentLabel.removeFromSupernode()
        }
        commentNodes.removeAll()
    }
    
    func createCommentLabels() -> Int{
        let addViewAllCommentsLabel = commentFeed!.numberOfCommentsForPhotoExceedsInteger(NUM_COMMENTS_TO_SHOW)
        let numCommentsInFeed = commentFeed!.numberOfItemsInFeed()
        let numLabelsToAdd = addViewAllCommentsLabel ? NUM_COMMENTS_TO_SHOW + 1 : numCommentsInFeed
        for var _ in 0...numLabelsToAdd - 1{
            let commentLabel = ASTextNode()
            commentLabel.maximumNumberOfLines = 3
            commentNodes.append(commentLabel)
            addSubnode(commentLabel)
        }
        return numLabelsToAdd;
    }
}
