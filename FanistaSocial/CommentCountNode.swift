//
//  CommentCountNode.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/31/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class CommentCountNode: ASCellNode{
    
    private var countNode: ASButtonNode
    
    init(counter: String){
        countNode = ASButtonNode()
        countNode.setAttributedTitle(NSAttributedString(string: counter), forState: .Normal)
        countNode.backgroundColor = buttonColor
        countNode.borderWidth = 1.0
        countNode.borderColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).CGColor
        countNode.contentEdgeInsets = UIEdgeInsetsMake(7, 11, 7, 11)
        super.init()
        addSubnode(countNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        countNode.preferredFrameSize = CGSize(width: 28, height: 28)
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: countNode)
    }
    
    
}