//
//  ProductItemDetailCell.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation

import UIKit
import AsyncDisplayKit

let cellIdentify = "cellIdentify"

class NTTableViewCell : UITableViewCell{
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.font = UIFont.systemFontOfSize(13)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        let imageView :UIImageView = self.imageView!;
//        imageView.frame = CGRectZero
//        if (imageView.image != nil) {
//            let imageHeight = imageView.image!.size.height*screenWidth/imageView.image!.size.width
//            imageView.frame = CGRectMake(0, 0, screenWidth, imageHeight)
//        }
    }
}

class ProductItemDetailCell : ASCellNode, ASTableDelegate, ASTableDataSource{
    var imageName : ImageModel
    var pullAction : ((offset : CGPoint) -> Void)?
    var tappedAction : (() -> Void)?
//    var imageNode: ASNetworkImageNode
    let tableNode: ASTableNode
//    init(imageUrl: String) {
//        imageName = imageUrl
//        tableNode = ASTableNode(style: .Plain)
        
//        backgroundColor = UIColor.lightGrayColor()
        
//        contentView.addSubview(tableView)
//        tableView.registerClass(NTTableViewCell.self, forCellReuseIdentifier: cellIdentify)
//        tableNode.delegate = self
//        tableNode.dataSource = self
//        addSubnode(tableNode)
        
//        super.init()
//        self.backgroundColor = UIColor.brownColor()
//        addSubnode(tableNode)
//    }
    
    init(imageModel: ImageModel){
        imageName = imageModel
//        imageNode = ASNetworkImageNode()
//        imageNode.URL = NSURL(string: imageName)
//        imageNode.preferredFrameSize = CGSize(width: screenWidth, height: 400)
        
        tableNode = ASTableNode(style: .Plain)
        
        super.init()
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.preferredFrameSize = CGSize(width: screenWidth, height: screenHeight - navigationHeaderAndStatusbarHeight - tabbarHeight)
        tableNode.frame = CGRectMake(0, navigationHeaderAndStatusbarHeight, screenWidth, screenHeight - navigationHeaderAndStatusbarHeight - tabbarHeight)
//        tableNode.backgroundColor = UIColor.greenColor()
//        addSubnode(imageNode)
        addSubnode(tableNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let spec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [tableNode])
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: spec)
    }
    
//    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
//        let layoutSpec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [tableNode])
//        return ASInsetLayoutSpec(
//            insets: UIEdgeInsetsMake(0, 0, 0, 0),
//            child: layoutSpec)
//    }
    
    override func didLoad() {
        super.didLoad()
//        tableNode.view.automaticallyAdjustsContentOffset = false
//        tableNode.view.pagingEnabled = true
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        tableView.reloadData()
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: ASTableView, nodeForRowAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let cell = ProductDetailImageSliderCell(imgUrl: imageName)
//        cell.preferredFrameSize = CGSize(width: screenWidth, height: screenHeight - tabbarHeight - navigationHeaderAndStatusbarHeight)
        return cell
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    
    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentify) as! NTTableViewCell!
//        cell.imageView?.image = nil
//        cell.textLabel?.text = nil
//        if indexPath.row == 0 {
//            let image = UIImage(named: imageName!)
//            cell.imageView?.image = image
//        }else{
//            cell.textLabel?.text = "try pull to pop view controller 😃"
//        }
//        cell.setNeedsLayout()
//        return cell
//    }
    
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
//        var cellHeight : CGFloat = navigationHeight
//        if indexPath.row == 0{
//            let image:UIImage! = UIImage(named: imageName!)
//            let imageHeight = image.size.height*screenWidth/image.size.width
//            cellHeight = imageHeight
//        }
//        return cellHeight
//        return 100
//    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        tappedAction?()
    }
    
    func scrollViewWillBeginDecelerating(scrollView : UIScrollView){
        if scrollView.contentOffset.y < navigationHeight{
            pullAction?(offset: scrollView.contentOffset)
        }
    }
}