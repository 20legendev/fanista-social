//
//  ProductItem.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/8/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//


import AsyncDisplayKit

protocol ProductItemProtocol {
    func didLoadFeatureImage(cell: ProductItem)
}

class ProductItem: ASCellNode, NTTansitionWaterfallGridViewProtocol{
    
//    private var parentDelegate: ProductItemProtocol
    private var priceNode: ASTextNode
    private var pictureNode: ASNetworkImageNode
    var _image: ImageModel
    init(image: ImageModel, text: String){
        _image = image
        pictureNode = ASNetworkImageNode()
        pictureNode.backgroundColor = UIColor.clearColor()
        pictureNode.URL = NSURL(string: image.url)
        pictureNode.contentMode = .ScaleAspectFill
        pictureNode.clipsToBounds = true
        pictureNode.cornerRadius = 7
//        parentDelegate = delegate
//        pictureNode.alpha = 0.5
        
        priceNode = ASTextNode()
        priceNode.attributedString = NSAttributedString(string: "120,000đ", attributes: [
            NSFontAttributeName: UIFont(name: "Helvetica", size: 13.0)!,
            NSForegroundColorAttributeName: UIColor.init(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
            ])
        priceNode.alignSelf = .Start
        super.init()
//        pictureNode.delegate = self
        addSubnode(pictureNode)
        addSubnode(priceNode)
    }
    
    func imageRatio(imageNode: ASNetworkImageNode) -> ASRatioLayoutSpec {
            return ASRatioLayoutSpec(ratio: CGFloat(_image.height) / CGFloat(_image.width), child: imageNode)
    }

    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let spec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [imageRatio(pictureNode)])
        let vspec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [spec,ASInsetLayoutSpec(
                insets: UIEdgeInsetsMake(5, 0, 5, 0),
                child: ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [priceNode]))])
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: vspec)
    }
    
}

//extension ProductItem: ASNetworkImageNodeDelegate{

//    func imageNode(imageNode: ASNetworkImageNode, didLoadImage image: UIImage) {
//        print("LOADED")
//        self.setNeedsLayout()
//    }
    
//    func imageNodeDidFinishDecoding(imageNode: ASNetworkImageNode) {
//        print("FINISHED")
//        if let node = imageNode.image{
//            print( node.size.width, node.size.height)
//            self.setNeedsLayout()
//            self.parentDelegate.didLoadFeatureImage(self)
//        }
//    }
//}

extension ProductItem{
    func snapShotForTransition() -> UIView! {
        let snapShotView = UIImageView(image: pictureNode.image)
        snapShotView.frame = pictureNode.frame
        return snapShotView
    }
}