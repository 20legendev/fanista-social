//
//  TimelineItemDetailCell.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/30/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class TimelineItemDetailCell: ASCellNode {
    private var mainPictureNode: ASNetworkImageNode!
    private var pictureMain: ImageModel
    private var ratio: CGFloat
    
    private var avatarNode: ASNetworkImageNode
    private var usernameNode: ASTextNode
    private var timeAgoNode: ASTextNode
    
//    private var captionNode: ASTextNode
    private var _row: Int?
    private var commentNode: CommentsNode
    
    init(topText: String, captionText: String?, picture: ImageModel, row: Int) {
        pictureMain = picture
        ratio = pictureMain.height/pictureMain.width
        
        usernameNode = ASTextNode()
        usernameNode.layerBacked = true
        var text = "ParisLady"
        text.replaceRange(text.startIndex...text.startIndex, with: String(text[text.startIndex]).capitalizedString)
        usernameNode.attributedString = NSAttributedString(string: text, attributes: [
            NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 15.0)!,
            NSForegroundColorAttributeName: textColor
        ])
        
        
        avatarNode = ASNetworkImageNode()
        avatarNode.preferredFrameSize = CGSizeMake(36, 36)
        avatarNode.backgroundColor = UIColor.clearColor()
        avatarNode.URL = NSURL(string: "https://howshost.com/wp-content/uploads/2015/11/create-custom-default-avatar-50x50.jpg")
        avatarNode.cornerRadius = 18
        
        
        timeAgoNode = ASTextNode()
        timeAgoNode.attributedString = NSAttributedString(string: "9 phút trước", attributes: [
            NSForegroundColorAttributeName: UIColor.init(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
            ])
        timeAgoNode.flexShrink = true
        _row = row
        
//        captionNode = TimelineItemDetailCell.createCaption("Sáng mát mẻ diện em phông kẻ này đi chơi thì hết ý òi 👍👍 màu nào cũng dễ mix đồ hết luôn, chất dày dặn mát mẻ cực. #‎170k cho một em, mưa gió mọi người chỉ việc ngồi nhà nhấc máy alo để được ship hàng nhe, #‎freeshipHN\n❤️50A ngõ 69 Chùa Láng \n❤️0975970443 - 0976039963")
        commentNode = CommentsNode(row: 0)
        super.init()
        addSubnode(commentNode)
        mainPictureNode = createMainPicture(imageWithURL: pictureMain.url)
        addSubnode(mainPictureNode)
        addSubnode(usernameNode)
        addSubnode(avatarNode)
        addSubnode(timeAgoNode)
        selectionStyle = .None
//        addSubnode(captionNode)
        
        commentNode.updateWithCommentFeedModel(CommentFeedModel(_comments: [CommentModel(id: "", commentId: "1", commentUsername: "tonsurton_store", commentAvatarUrl: "", body: "Sáng mát mẻ diện em phông kẻ này đi chơi thì hết ý òi 👍👍 màu nào cũng dễ mix đồ hết luôn, chất dày dặn mát mẻ cực. #‎170k cho một em, mưa gió mọi người chỉ việc ngồi nhà nhấc máy alo để được ship hàng nhe, #‎freeshipHN\n❤️50A ngõ 69 Chùa Láng \n❤️0975970443 - 0976039963", uploadDateString: "", uploadDateRaw: ""),CommentModel(id: "", commentId: "1", commentUsername: "kiennt07", commentAvatarUrl: "", body: "Em ơi hàng còn không", uploadDateString: "", uploadDateRaw: ""),CommentModel(id: "", commentId: "1", commentUsername: "kiennt07", commentAvatarUrl: "", body: "Em ơi hàng còn không", uploadDateString: "", uploadDateRaw: ""),CommentModel(id: "", commentId: "1", commentUsername: "kiennt07", commentAvatarUrl: "", body: "Em ơi hàng còn không", uploadDateString: "", uploadDateRaw: ""),CommentModel(id: "", commentId: "1", commentUsername: "kiennt07", commentAvatarUrl: "", body: "Em ơi hàng còn không", uploadDateString: "", uploadDateRaw: "")], _photoID: "", _urlString: "", _currentPage: 0, _totalPages: 1, _totalItems: 4, _fetchPageInProgress: false, _refreshFeedInProgress: false))
        
    }
    
    static func createCaption(captionText: String) -> ASTextNode{
        let node = ASTextNode()
        node.layerBacked = true
        
        let captionAttribute = [NSFontAttributeName:UIFont(
            name: "HelveticaNeue",
            size: 14.0)!,
                                NSForegroundColorAttributeName: textColor]
        
        let cText = NSAttributedString(string: captionText, attributes: captionAttribute)
        node.maximumNumberOfLines = 0
        node.attributedString = cText
        return node
    }
    
    func createMainPicture(imageWithURL URL: String) -> ASNetworkImageNode {
        let image = ASNetworkImageNode()
        image.URL = NSURL(string: URL)
        image.preferredFrameSize = CGSize(width: screenWidth, height: pictureMain.height * screenWidth/pictureMain.width)
        image.backgroundColor = UIColor.clearColor()
        image.contentMode = .ScaleAspectFill
        image.clipsToBounds = true
        return image;
    }

    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackSpec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Start, children: [ commentNode])
        let layoutCaptionSpec = ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(16, 16, 16, 16),
            child: stackSpec)
        
        let layoutSpec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: Array.filterNils([mainPictureNode, layoutCaptionSpec]))
        
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: layoutSpec)
    }
}