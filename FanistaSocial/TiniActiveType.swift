//
//  TiniActiveType.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 8/23/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
public enum TiniActiveType {
    case Mention
    case Hashtag
    case URL
    case Custom(pattern: String)
    
    var pattern: String {
        switch self {
        case .Mention: return RegexParser.mentionPattern
        case .Hashtag: return RegexParser.hashtagPattern
        case .URL: return RegexParser.urlPattern
        case .Custom(let regex): return regex
        }
    }
}