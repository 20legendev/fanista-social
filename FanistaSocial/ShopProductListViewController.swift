//
//  ShopProductListViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/7/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//


import AsyncDisplayKit
import SwiftyJSON

protocol TiniCollectionNodeProtocol {
    func getTiniCollectionNode() -> ASCollectionNode
    func getLastContentOffset() -> CGPoint
}

//class NavigationControllerDelegate: NSObject, UINavigationControllerDelegate{
//    
//    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?{
//        
//        let fromVCConfromA = (fromVC as? NTTransitionProtocol)
//        let fromVCConfromB = (fromVC as? NTWaterFallViewControllerProtocol)
//        let fromVCConfromC = (fromVC as? NTHorizontalPageViewControllerProtocol)
//        
//        let toVCConfromA = (toVC as? NTTransitionProtocol)
//        let toVCConfromB = (toVC as? NTWaterFallViewControllerProtocol)
//        let toVCConfromC = (toVC as? NTHorizontalPageViewControllerProtocol)
//        if((fromVCConfromA != nil)&&(toVCConfromA != nil)&&(
//            (fromVCConfromB != nil && toVCConfromC != nil)||(fromVCConfromC != nil && toVCConfromB != nil))){
//            let transition = NTTransition()
//            transition.presenting = operation == .Pop
//            return transition
//        }else{
//            return nil
//        }
//    }
//}

class ShopProductListViewController: ASViewController, ASCollectionDelegate, ASCollectionDataSource, TiniCollectionNodeProtocol{
    var collectionNode: ASCollectionNode?
    var lastContentOffset: CGPoint = CGPointZero
    var _layoutInspector: MosaicCollectionViewLayoutInspector?
    var arr = [
        "http://img0.ropose.com/clothes.jpeg",
        "http://img0.ropose.com/womenbags.jpeg",
        "http://img0.ropose.com/womenshoes.jpeg",
        "http://img0.ropose.com/womenaccessories.jpeg",
        "http://img0.ropose.com/jewellery.jpeg",
        "http://img0.ropose.com/lingerie.jpeg",
        "http://img0.ropose.com/beauty.jpeg",
        "http://img0.ropose.com/everything.jpeg"
    ]
    var prd = [
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/06/09/ung-dung-chat-lieu-luoi-phoi-do-thoi-trang-he-em-dep-2-000547049.jpg", "width": 600, "height": 434])),
        ImageModel(json: JSON(["url": "http://thumb.connect360.vn/unsafe/645x0/imgs.emdep.vn/Share/Image/2016/06/08/1-114240314.jpg", "width": 251, "height": 378 ])),
        ImageModel(json: JSON(["url": "https://static.thitruongsi.com/image/cached/size/1280/0/img/product/2016/02/25/56ce6fb9b1177.jpg", "width": 253, "height": 378])),
        ImageModel(json: JSON(["url": "http://static-vn.zacdn.com/p/zalora-6715-692765-1.jpg", "width": 261, "height": 378 ])),
        ImageModel(json: JSON(["url": "http://thumb.connect360.vn/unsafe/645x0/imgs.emdep.vn/Share/Image/2016/06/08/7-114240346.jpg", "width": 251, "height": 378 ]))
        ,
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/06/09/ung-dung-chat-lieu-luoi-phoi-do-thoi-trang-he-em-dep-2-000547049.jpg", "width": 522, "height": 386])),
        ImageModel(json: JSON(["url": "http://thumb.connect360.vn/unsafe/645x0/imgs.emdep.vn/Share/Image/2016/06/08/1-114240314.jpg", "width": 251, "height": 378 ])),
        ImageModel(json: JSON(["url": "https://static.thitruongsi.com/image/cached/size/1280/0/img/product/2016/02/25/56ce6fb9b1177.jpg", "width": 253, "height": 378])),
        ImageModel(json: JSON(["url": "http://static-vn.zacdn.com/p/velvet-2900-843525-1.jpg", "width": 261, "height": 378 ])),
        ImageModel(json: JSON(["url": "http://thumb.connect360.vn/unsafe/645x0/imgs.emdep.vn/Share/Image/2016/06/08/7-114240346.jpg", "width": 251, "height": 378 ])),
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/06/09/ung-dung-chat-lieu-luoi-phoi-do-thoi-trang-he-em-dep-2-000547049.jpg", "width": 522, "height": 386])),
        ImageModel(json: JSON(["url": "http://thumb.connect360.vn/unsafe/645x0/imgs.emdep.vn/Share/Image/2016/06/08/1-114240314.jpg", "width": 251, "height": 378 ])),
        ImageModel(json: JSON(["url": "https://static.thitruongsi.com/image/cached/size/1280/0/img/product/2016/02/25/56ce6fb9b1177.jpg", "width": 253, "height": 378])),
        ImageModel(json: JSON(["url": "http://static-vn.zacdn.com/p/zalora-6715-692765-1.jpg", "width": 261, "height": 378 ]))
    ]
    
//    let delegateHolder = NavigationControllerDelegate()
    
    init() {
        let _layout = MosaicCollectionViewLayout()
        _layout.numberOfColumns = 2
        _layout.headerHeight = 166
        _layout.columnSpacing = pinPadding
        
        _layoutInspector = MosaicCollectionViewLayoutInspector()
        _layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        _layout.headerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
//        _layout.interItemSpacing = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        
        let _node = ASCollectionNode(collectionViewLayout: _layout)
//        _node.backgroundColor = UIColor.whiteColor()
        super.init(node: _node)
        _node.delegate = self
        _node.dataSource = self
        collectionNode = _node
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionNode!.view.layoutInspector = _layoutInspector
        collectionNode!.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
//        collectionNode!.view.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        self.navigationController!.delegate = delegateHolder
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let cell = ProductItem(image: prd[indexPath.row], text: "Huy remi")
        cell.setNeedsLayout()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prd.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: ASCollectionView, constrainedSizeForNodeAtIndexPath indexPath: NSIndexPath) -> ASSizeRange {
        return ASSizeRangeMake(CGSize(width: gridWidth, height: 0), CGSize(width: gridWidth, height: 1000))
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let header = CategoryHorizontalList(count: 1)
        return header
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let pageViewController =
            ShopProductDetailViewController(collectionViewLayout: pageViewControllerLayout(), currentIndexPath:indexPath)
        pageViewController.imageNameList = prd
//        collectionView.setToIndexPath(indexPath)
//        lastContentOffset = collectionView.contentOffset
        navigationController!.pushViewController(pageViewController, animated: true)
    }
    
    func pageViewControllerLayout () -> UICollectionViewFlowLayout {
        let flowLayout = UICollectionViewFlowLayout()
        let itemSize  = self.navigationController!.navigationBarHidden ?
            CGSizeMake(screenWidth, screenHeight + 20) : CGSizeMake(screenWidth, screenHeight - navigationHeaderAndStatusbarHeight)
        flowLayout.itemSize = itemSize
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.scrollDirection = .Horizontal
        return flowLayout
    }

//    func didLoadFeatureImage(cell: ProductItem) {
//        self.collectionNode?.beginUpdates()
//        cell.setNeedsLayout()
//        self.collectionNode?.endUpdatesAnimated(false)
//        collectionNode?.reloadData()
//        self.collectionNode?.endUpdatesAnimated(false)
//    }
    
    func getLastContentOffset() -> CGPoint {
        return self.lastContentOffset
    }
    
}


extension ShopProductListViewController{
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-button"), style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    func getTiniCollectionNode() -> ASCollectionNode{
        return self.collectionNode!
    }

}
extension ShopProductListViewController: MosaicCollectionViewLayoutDelegate {
    
    func collectionView(collectionView:UICollectionView, layout: MosaicCollectionViewLayout, originalItemSizeAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let w =  CGFloat(prd[indexPath.row % prd.count].width)
        let h =  CGFloat(prd[indexPath.row % prd.count].height)
        return CGSize(width: gridWidth, height: gridWidth * (h/w) + 20)
    }
    
}

extension ShopProductListViewController: NTTransitionProtocol , NTWaterFallViewControllerProtocol{
    
    func transitionCollectionView() -> ASCollectionNode!{
        return collectionNode
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        lastContentOffset = scrollView.contentOffset
    }

    func viewWillAppearWithPageIndex(pageIndex : NSInteger) {
        var position : UICollectionViewScrollPosition = UICollectionViewScrollPosition.CenteredHorizontally.intersect(.CenteredVertically)
        let image : ASNetworkImageNode! = ASNetworkImageNode()
        image.URL = NSURL(string: prd[pageIndex].url)
        let imageHeight = prd[pageIndex].height * gridWidth/prd[pageIndex].width
        if imageHeight > 1000 {//whatever you like, it's the max value for height of image
            position = .Top
        }
        let currentIndexPath = NSIndexPath(forRow: pageIndex, inSection: 0)
        let collectionView = self.collectionNode;
        print(currentIndexPath)
        collectionView!.view.setToIndexPath(currentIndexPath)
        if pageIndex < 2{
            collectionView!.view.setContentOffset(CGPointZero, animated: false)
        }else{
            collectionView!.view.scrollToItemAtIndexPath(currentIndexPath, atScrollPosition: position, animated: false)
        }
    }


}
