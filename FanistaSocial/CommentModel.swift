//
//  CommentModel.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 8/23/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommentModel: NSObject, NSCoding, BaseInitProtocol{
    
    var _ID: String
    var _commenterID : String
    var _commenterUsername : String
    var _commenterAvatarURL: String
    var _body: String
    var _uploadDateString: String
    var _uploadDateRaw: String

    init(id: String, commentId: String, commentUsername: String, commentAvatarUrl: String, body: String, uploadDateString: String, uploadDateRaw: String){
        _ID = id
        _commenterID = commentId
        _commenterUsername = commentUsername
        _commenterAvatarURL = commentAvatarUrl
        _body = body
        _uploadDateString = uploadDateString
        _uploadDateRaw = uploadDateRaw
        super.init()
    }
    
    
    required init(json: JSON) {
        _ID = json["_ID"].stringValue
        _commenterID = json["_commenterID"].stringValue
        _commenterUsername = json["_commenterUsername"].stringValue
        _commenterAvatarURL = json["_commenterAvatarURL"].stringValue
        _body = json["_body"].stringValue
        _uploadDateString = json["_uploadDateString"].stringValue
        _uploadDateRaw = json["_uploadDateRaw"].stringValue
    }
    
    required init?(coder decoder: NSCoder) {
        _ID = decoder.decodeObjectForKey("_ID") as! String
        _commenterID = decoder.decodeObjectForKey("_commenterID") as! String
        _commenterUsername = decoder.decodeObjectForKey("_commenterUsername") as! String
        _commenterAvatarURL = decoder.decodeObjectForKey("_commenterAvatarURL") as! String
        _body = decoder.decodeObjectForKey("_body") as! String
        _uploadDateString = decoder.decodeObjectForKey("_uploadDateString") as! String
        _uploadDateRaw = decoder.decodeObjectForKey("_uploadDateRaw") as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(_ID, forKey: "_ID")
        aCoder.encodeObject(_commenterID, forKey: "_commenterID")
        aCoder.encodeObject(_commenterUsername, forKey: "_commenterUsername")
        aCoder.encodeObject(_commenterAvatarURL, forKey: "_commenterAvatarURL")
        aCoder.encodeObject(_body, forKey: "_body")
        aCoder.encodeObject(_uploadDateString, forKey: "_uploadDateString")
        aCoder.encodeObject(_uploadDateRaw, forKey: "_uploadDateRaw")
    }
    
    func commentAttributedString() -> NSAttributedString {
        let string = NSString(format: "%@ %@", _commenterUsername.lowercaseString, _body)
        let formattedString = NSMutableAttributedString()
        formattedString.bold("\(_commenterUsername.lowercaseString) ", color: textColor).normal(" \(_body)", color: textColor)
        return formattedString
    }

}
