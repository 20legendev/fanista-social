//
//  Macro.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation

let screenBounds = UIScreen.mainScreen().bounds
let screenSize   = screenBounds.size
let screenWidth  = screenSize.width
let screenHeight = screenSize.height
let pinPadding : CGFloat = 10.0
let gridWidth : CGFloat = (screenSize.width - pinPadding * 3 ) / 2
let navigationHeight : CGFloat = 44.0
let statubarHeight : CGFloat = 20.0
let tabbarHeight : CGFloat = 49.0
let navigationHeaderAndStatusbarHeight : CGFloat = navigationHeight + statubarHeight
let screenInsideHeight: CGFloat = screenHeight - navigationHeaderAndStatusbarHeight - tabbarHeight

let textColor: UIColor = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
let buttonColor: UIColor = UIColor.init(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)