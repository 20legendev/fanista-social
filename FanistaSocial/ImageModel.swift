//
//  ImageModel.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import SwiftyJSON

class ImageModel: NSObject, NSCoding, BaseInitProtocol{
    var url: String
    var width: CGFloat
    var height: CGFloat
    
    required init(json: JSON) {
    
        url = json["url"].stringValue
        width = CGFloat(json["width"].floatValue)
        height = CGFloat(json["height"].floatValue)
    }
    
    required init?(coder decoder: NSCoder) {
        url = decoder.decodeObjectForKey("url") as! String
        width = decoder.decodeObjectForKey("width") as! CGFloat
        height = decoder.decodeObjectForKey("height") as! CGFloat
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(url, forKey: "url")
        aCoder.encodeObject(width, forKey: "width")
        aCoder.encodeObject(height, forKey: "height")
    }
}
