//
//  AppDelegate.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/28/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import UIKit
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        UINavigationBar.setupStyle()
//        let window = WindowWithStatusBarUnderlay(frame: UIScreen.mainScreen().bounds)
        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window.backgroundColor = UIColor.whiteColor()
        window.makeKeyAndVisible()
        self.window = window
        
        setupTabbar()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func setupTabbar(){
        let timelineVC = TimelineViewController()
        let timelineNavCtrl = UINavigationController.init(rootViewController: timelineVC)
        timelineNavCtrl.tabBarItem = UITabBarItem.init(title: "Timeline", image: UIImage(named: "home"), tag:
            1)
        timelineNavCtrl.tabBarItem.selectedImage = UIImage(named: "home_selected");
//        timelineNavCtrl.hidesBarsOnSwipe = true
        
        let explorerVC = ExplorerViewController()
        let explorereNav = UINavigationController.init(rootViewController: explorerVC)
        
        explorereNav.tabBarItem = UITabBarItem.init(title: "Explore", image: UIImage(named: "home"), tag:
            1)
        explorereNav.tabBarItem.selectedImage = UIImage(named: "home_selected");
        
        let shopVc = ShopViewController()
//        let shopNav = NTNavigationController.init(rootViewController: shopVc)
        let shopNav = UINavigationController.init(rootViewController: shopVc)
        
        shopNav.tabBarItem = UITabBarItem.init(title: "Shop", image: UIImage(named: "clothes"), tag:
            1)
        shopNav.tabBarItem.selectedImage = UIImage(named: "home_selected");
        
        let tabbarCtrl = UITabBarController()
        tabbarCtrl.viewControllers = [timelineNavCtrl, explorereNav, shopNav]
        tabbarCtrl.selectedViewController = timelineNavCtrl
        tabbarCtrl.delegate = self
        UITabBar.appearance().tintColor = UIColor.blackColor()
        
        self.window!.rootViewController = tabbarCtrl;
    }


    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

