//
//  Base.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 3/18/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//
import UIKit
import Foundation
import SwiftyJSON

extension Array {

    static func filterNils(array: [Element?]) -> [Element] {
        return array.filter { $0 != nil }.map { $0! }
    }
    
}

extension UIView{
    func origin (point : CGPoint){
        frame.origin.x = point.x
        frame.origin.y = point.y
    }
}

var kIndexPathPointer = "kIndexPathPointer"

extension UICollectionView{
    
    func setToIndexPath (indexPath : NSIndexPath){
        objc_setAssociatedObject(self, &kIndexPathPointer, indexPath, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func toIndexPath () -> NSIndexPath {
        let index = self.contentOffset.x/self.frame.size.width
        if index > 0{
            return NSIndexPath(forRow: Int(index), inSection: 0)
        }else if let indexPath = objc_getAssociatedObject(self,&kIndexPathPointer) as? NSIndexPath {
            return indexPath
        }else{
            return NSIndexPath(forRow: 0, inSection: 0)
        }
    }
    
    func fromPageIndexPath () -> NSIndexPath{
        let index : Int = Int(self.contentOffset.x/self.frame.size.width)
        return NSIndexPath(forRow: index, inSection: 0)
    }
}

func setTimeout(delay:NSTimeInterval, block:()->Void) -> NSTimer {
    return NSTimer.scheduledTimerWithTimeInterval(delay, target: NSBlockOperation(block: block), selector: "main", userInfo: nil, repeats: false)
}

func setInterval(interval:NSTimeInterval, block:()->Void) -> NSTimer {
    return NSTimer.scheduledTimerWithTimeInterval(interval, target: NSBlockOperation(block: block), selector: "main", userInfo: nil, repeats: true)
}

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect, fromColor: UIColor, toColor: UIColor) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [fromColor.CGColor, toColor.CGColor]
        return layer
    }
}

extension UIImage{
    class func imageLayerForGradientBackground(frame: CGRect, fromColor: UIColor, toColor: UIColor) -> UIImage {
        var updatedFrame = frame
        updatedFrame.size.height += 20
        var layer = CAGradientLayer.gradientLayerForBounds(updatedFrame, fromColor: fromColor, toColor: toColor)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

protocol BaseInitProtocol {
    init(json: JSON)
}

struct ModelArrayProvider {
    static func arrayModel<T: BaseInitProtocol>(anyClass: AnyClass,json: JSON) -> [T]? {
        guard json != nil else {
            return nil
        }
        if anyClass is T.Type {
            let model = anyClass as! T.Type
            var array = [T]()
            for (_, dict) in json {
                array.append(model.init(json: dict))
            }
            return array
        }else {
            return nil
        }
    }
    
    static func arrayWithJson(json: JSON) -> [String]?{
        
        guard json != nil else {
            return nil
        }
        
        var array: [String] = []
        
        for (_, dict) in json {
            array.append(dict.stringValue)
        }
        return array
    }
}
extension NSMutableAttributedString {
    func bold(text:String, color: UIColor) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "HelveticaNeue-Bold", size: 14)!, NSForegroundColorAttributeName: color]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.appendAttributedString(boldString)
        return self
    }
    
    func normal(text:String, color: UIColor)->NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 14)!, NSForegroundColorAttributeName: color]
        let normal =  NSAttributedString(string: text, attributes: attrs)
        self.appendAttributedString(normal)
        return self
    }
}
