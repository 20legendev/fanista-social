//
//  ShopProductDetailViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

let horizontalPageViewCellIdentify = "horizontalPageViewCellIdentify"

class ShopProductDetailViewController : ASViewController, ASCollectionDelegate, ASCollectionDataSource, PopViewProtocol{
    var collectionNode: ASCollectionNode
    var imageNameList : Array <ImageModel> = []
    var pullOffset = CGPointZero
    var _indexPath: NSIndexPath
    
    init(collectionViewLayout layout: UICollectionViewLayout!, currentIndexPath indexPath: NSIndexPath){
        collectionNode = ASCollectionNode(collectionViewLayout: layout)
        _indexPath = indexPath
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.collectionNode.view.pagingEnabled = true
        //        self.collectionNode.view.registerClass(ProductItemDetailCell.self, forCellWithReuseIdentifier: horizontalPageViewCellIdentify)
        self.collectionNode.view.setToIndexPath(_indexPath)
        self.collectionNode.view.performBatchUpdates({self.collectionNode.view.reloadData()}, completion: { finished in
            if finished {
                self.collectionNode.view.scrollToItemAtIndexPath(self._indexPath, atScrollPosition:.CenteredHorizontally, animated: false)
            }});
//        self.collectionNode.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
//        if indexPath.row == 0{
            let cell = ProductItemDetailCell(imageModel: imageNameList[indexPath.row] )
            return cell;
//        }
//        let cell = ASCellNode()
//        cell.preferredFrameSize = CGSize(width: 320, height: 200)
//        cell.backgroundColor = UIColor.brownColor()
//        return cell

//        let collectionCell: ProductItemDetailCell = ProductItemDetailCell(imageUrl: (self.imageNameList[indexPath.row]).url)
        
//        collectionCell.tappedAction = {}
//        cell.pullAction = { offset in
//            self.pullOffset = offset
//            self.navigationController!.popViewControllerAnimated(true)
//        }
//        collectionCell.setNeedsLayout()
//        collectionCell.preferredFrameSize = CGSize(width: 320, height: 200)
//        return collectionCell
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageNameList.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: ASCollectionView, constrainedSizeForNodeAtIndexPath indexPath: NSIndexPath) -> ASSizeRange {
        return ASSizeRangeMake(CGSize(width: screenWidth, height: 0), CGSize(width: screenWidth, height: 1000))
    }
    
//    func collectionView(collectionView: ASCollectionView, nodeForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> ASCellNode{
//        let header = CategoryHorizontalList(count: 1)
//        return header
//    }
    
}

extension ShopProductDetailViewController: NTTransitionProtocol, NTHorizontalPageViewControllerProtocol{
    
    func transitionCollectionView() -> ASCollectionNode!{
        return self.collectionNode
    }
    
    func pageViewCellScrollViewContentOffset() -> CGPoint{
        return self.pullOffset
    }
    
    func getCollectionNode() -> ASCollectionNode! {
        return self.collectionNode
    }

}