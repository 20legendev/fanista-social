//
//  UINavigationBar.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/2/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
extension UINavigationBar {
    
    class func setupStyle(){
        let img = UIImage()
        let navBar = UINavigationBar.appearance()
        navBar.translucent = false
        navBar.shadowImage = img
        navBar.setBackgroundImage(img, forBarMetrics: .Default)
        navBar.barTintColor = UIColor.whiteColor()
        navBar.tintColor = UIColor(red: 85/255, green:  85/255, blue:  85/255, alpha: 1)
        let attr = [
            NSForegroundColorAttributeName : UIColor.whiteColor()
        ]
        navBar.titleTextAttributes = attr
    }

}
