//
//  TimelineDetailViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/29/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SwiftyJSON
import MXParallaxHeader

class TimelineDetailViewController: ASViewController, ASCollectionDataSource, ASCollectionDelegate{
    
    var model: ImageModel?
    
    private var featureImage: TimelineItemDetailCell?
    private var originalHeight = CGFloat(0.0)
    private var imageOriginHeight = CGFloat(0.0)

    var collectionNode: ASCollectionNode {
        return node as! ASCollectionNode
    }

    init() {
//        let flowLayout = StickyHeaderFlowLayout()
        let flowLayout = UICollectionViewFlowLayout()
//        flowLayout.headerReferenceSize = CGSizeMake(screenWidth, 50);
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        flowLayout.minimumInteritemSpacing  = 7
        flowLayout.minimumLineSpacing       = 7
    
        // flowLayout.headerReferenceSize = CGSizeMake(screenWidth, 52.0);
        
        let node = ASCollectionNode(collectionViewLayout: flowLayout)
        flowLayout.minimumInteritemSpacing  = 10
        flowLayout.minimumLineSpacing       = 10

        super.init(node: node)
        node.delegate = self
        node.dataSource = self
        
//        node.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
//        node.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        collectionNode.frame = CGRect(x: 0, y: -64, width: screenWidth, height: screenHeight)
//        collectionNode.view.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
//        tableNode.clipsToBounds = false
        
//        if let imgModel = model{
//            let view = ASNetworkImageNode()
//            view.URL = NSURL(string: imgModel.url)
//            view.contentMode = .ScaleAspectFill
        
//            var view = UIImageView(image: UIImage(named: "1"))
//            self.collectionNode.view.parallaxHeader.view = view.view
//            self.collectionNode.view.parallaxHeader.height = (screenWidth * imgModel.height)/imgModel.width
//            self.collectionNode.view.parallaxHeader.mode = MXParallaxHeaderMode.Fill
//            self.collectionNode.view.parallaxHeader.minimumHeight = 0
//        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        setupNavigationBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
//        self.setupNavigationBarDisappear()
//        navigationController?.navigationBar.barStyle = .Default
//        navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
//        self.navigationController?.view.backgroundColor = UIColor.whiteColor()
//        UINavigationBar.setupStyle();
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }

    func setupNavigationBar(){
        let navBar: UINavigationBar! = navigationController?.navigationBar
        navBar.shadowImage = UIImage()
        navBar.translucent = true
        navBar.backgroundColor = UIColor.clearColor()
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        navBar.barStyle = .BlackTranslucent
        navBar.setBackgroundImage(UIImage.imageLayerForGradientBackground(navBar.bounds, fromColor: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha:0.2), toColor: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha:0)), forBarMetrics: UIBarMetrics.Default)

    }
    
    func setupNavigationBarDisappear(){
        let navBar: UINavigationBar! = navigationController?.navigationBar
        let img = UIImage()
        navBar.translucent = false
        navBar.shadowImage = img
        navBar.setBackgroundImage(img, forBarMetrics: .Default)
        navBar.barStyle = .Default
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode {
        let node = TimelineItemDetailCell(topText: "Bo nong", captionText: "Đù má", picture: model!, row: indexPath.row)
        print(model!.url)
        if indexPath.row == 0{
            featureImage = node
            originalHeight = node.frame.height
            print("height \(originalHeight)")
        }
        return node
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    /*
    func collectionView(collectionView: ASCollectionView, nodeForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let header = TimelineItemHeader(topText: "In here")
        return header
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

