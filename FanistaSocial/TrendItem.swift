//
//  TrendItem.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/7/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import AsyncDisplayKit

class TrendItem: ASCellNode{
    
    private var pictureNode: ASNetworkImageNode
    private var nameNode: ASTextNode
    
    init(imageUrl: String, text: String){
        
        pictureNode = ASNetworkImageNode()
        pictureNode.preferredFrameSize = CGSizeMake(72, 72)
        pictureNode.backgroundColor = UIColor.clearColor()
        pictureNode.URL = NSURL(string: imageUrl)
        pictureNode.contentMode = .ScaleAspectFill
        pictureNode.clipsToBounds = true
        
        nameNode = ASTextNode()
        nameNode.attributedString = NSAttributedString(string: text.uppercaseString, attributes: [
            NSFontAttributeName: UIFont(name: "Arial", size: 16.0)!,
            NSForegroundColorAttributeName: UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
            ])
        nameNode.alignSelf = .Center
        
        super.init()
        addSubnode(pictureNode)
        addSubnode(nameNode)
    }
    
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let spec = ASRelativeLayoutSpec(horizontalPosition: .End, verticalPosition: .End, sizingOption: .MinimumWidth, child: nameNode)
        let layoutSpec = ASOverlayLayoutSpec(child: pictureNode, overlay: ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 9, 9),
            child: spec))
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: layoutSpec)
    }
    
}