//
//  CommentFeedModel.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 8/23/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommentFeedModel: NSObject, NSCoding, BaseInitProtocol{
    var comments: [CommentModel]
    var photoID: String
    var urlString: String
    var currentPage: Int
    var totalPages: Int
    var totalItems: Int
    var fetchPageInProgress: Bool
    var refreshFeedInProgress: Bool
    
    init(_comments: [CommentModel], _photoID: String, _urlString: String, _currentPage: Int, _totalPages: Int, _totalItems: Int, _fetchPageInProgress: Bool, _refreshFeedInProgress: Bool){
        comments = _comments
        photoID = _photoID
        urlString = _urlString
        currentPage = _currentPage
        totalPages = _totalPages
        totalItems = _totalItems
        fetchPageInProgress = _fetchPageInProgress
        refreshFeedInProgress = _refreshFeedInProgress
        super.init()
    }
    
    required init(json: JSON) {
        comments = ModelArrayProvider.arrayModel(NSObject.self, json: json["comments"])!
        photoID = json["photoID"].stringValue
        urlString = json["urlString"].stringValue
        currentPage = json["currentPage"].intValue
        totalPages = json["totalPages"].intValue
        totalItems = json["totalItems"].intValue
        fetchPageInProgress = json["fetchPageInProgress"].boolValue
        refreshFeedInProgress = json["refreshFeedInProgress"].boolValue
    }
    
    required init?(coder decoder: NSCoder) {
        comments = decoder.decodeObjectForKey("comments") as! [CommentModel]
        photoID = decoder.decodeObjectForKey("photoID") as! String
        urlString = decoder.decodeObjectForKey("urlString") as! String
        currentPage = decoder.decodeObjectForKey("currentPage") as! Int
        totalPages = decoder.decodeObjectForKey("totalPages") as! Int
        totalItems = decoder.decodeObjectForKey("totalItems") as! Int
        fetchPageInProgress = decoder.decodeObjectForKey("fetchPageInProgress") as! Bool
        refreshFeedInProgress = decoder.decodeObjectForKey("refreshFeedInProgress") as! Bool

    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(comments, forKey: "comments")
        aCoder.encodeObject(photoID, forKey: "photoID")
        aCoder.encodeObject(urlString, forKey: "urlString")
        aCoder.encodeObject(currentPage, forKey: "currentPage")
        aCoder.encodeObject(totalPages, forKey: "totalPages")
        aCoder.encodeObject(totalItems, forKey: "totalItems")
        aCoder.encodeObject(fetchPageInProgress, forKey: "fetchPageInProgress")
        aCoder.encodeObject(refreshFeedInProgress, forKey: "refreshFeedInProgress")
    }
    
    func numberOfCommentsForPhotoExceedsInteger(number: Int) -> Bool {
        return totalItems > number
    }
    
    func numberOfItemsInFeed() -> Int{
        return comments.count
    }
    func viewAllCommentsAttributedString() -> NSAttributedString{
        let string = "View all \(totalItems) comments"
        let attrString = NSAttributedString(string: string as String, attributes: [
            NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 14.0)!,
            NSForegroundColorAttributeName: UIColor.init(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
        ])
        return attrString;
    }

}
