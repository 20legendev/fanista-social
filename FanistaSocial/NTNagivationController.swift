//
//  NTNagivationController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/15/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import UIKit

class NTNavigationController : UINavigationController{
    
    override func popViewControllerAnimated(animated: Bool) -> UIViewController {
        let childrenCount = self.viewControllers.count
        if let toViewController = self.viewControllers[childrenCount-2] as? NTWaterFallViewControllerProtocol{
            let toView = toViewController.transitionCollectionView()
            let popedViewController = self.viewControllers[childrenCount-1] as! PopViewProtocol
            let popView  = popedViewController.getCollectionNode()
            let indexPath = popView.view.fromPageIndexPath()
            toViewController.viewWillAppearWithPageIndex(indexPath.row)
            toView.view.setToIndexPath(indexPath)
        }
        return super.popViewControllerAnimated(animated)!
    }
    
}