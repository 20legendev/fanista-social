//
//  ShopPlaceholderViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/6/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class ShopPlaceholderViewController: ASViewController, ASCollectionDelegate, ASCollectionDataSource{
    var collectionNode: ASCollectionNode?
    var parentNavigationController: UINavigationController?
    let sw = (screenWidth - 20 - 7) / 2

    var arr = [
        "http://img0.ropose.com/clothes.jpeg",
        "http://img0.ropose.com/womenbags.jpeg",
        "http://img0.ropose.com/womenshoes.jpeg",
        "http://img0.ropose.com/womenaccessories.jpeg",
        "http://img0.ropose.com/jewellery.jpeg",
        "http://img0.ropose.com/lingerie.jpeg",
        "http://img0.ropose.com/beauty.jpeg",
        "http://img0.ropose.com/everything.jpeg"
    ]
    
    init() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.headerReferenceSize = CGSizeMake(screenWidth, 50);
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        let node = ASCollectionNode(collectionViewLayout: flowLayout)
        flowLayout.minimumInteritemSpacing  = 7
        flowLayout.minimumLineSpacing       = 7
        
        super.init(node: node)
        node.delegate = self
        node.dataSource = self
        node.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
        self.collectionNode = node
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        if indexPath.section == 1{
            let cell = TrendItem(imageUrl: "http://img0.ropose.com/sundresses.jpeg", text: "Huy remi")
            return cell
        }else{
            let cell = CategoryItem(imageUrl: arr[indexPath.row], text: "Huy remi", sizeModel: 4)
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 8
        }
        return 12
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: ASCollectionView, constrainedSizeForNodeAtIndexPath indexPath: NSIndexPath) -> ASSizeRange {
        if indexPath.section == 1{
            return ASSizeRangeMake(CGSize(width: sw, height: sw), CGSize(width: sw, height: sw))
        }else{
            return ASSizeRangeMake(CGSize(width: 0, height: 0), CGSize(width: 100, height: 100))
        }
    }
    
    
    func collectionView(collectionView: ASCollectionView, nodeForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let header = ASTextCellNode()
        header.text = indexPath.section == 0 ? "Danh mục".uppercaseString : "Khám phá Xu hướng".uppercaseString
        header.textAttributes   = [NSFontAttributeName: UIFont(name: "Arial", size: 16.0)!,NSForegroundColorAttributeName: UIColor.init(red: 17/255, green: 17/255, blue: 17/255, alpha: 1.0)]
        header.textInsets       = UIEdgeInsets(top: 15, left: 10, bottom: 18, right: 10)
        return header
    }
}

extension ShopPlaceholderViewController{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        toDetailView()
    }
    
    func toDetailView(){
         let secondViewController = ShopProductListViewController()
         self.parentNavigationController!.pushViewController(secondViewController, animated: true)

    }
}