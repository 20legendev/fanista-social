//
//  CategoryItem.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/6/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import AsyncDisplayKit

class CategoryItem: ASCellNode{
    
    private var pictureNode: ASNetworkImageNode
    private var nameNode: ASTextNode
    
    init(imageUrl: String, text: String, sizeModel: CGFloat){
        pictureNode = ASNetworkImageNode()
        let size = floor((screenWidth - 20 - 21)/sizeModel)
        pictureNode.preferredFrameSize = CGSizeMake(size, size)
        pictureNode.backgroundColor = UIColor.clearColor()
        pictureNode.URL = NSURL(string: imageUrl)
        pictureNode.contentMode = .ScaleAspectFill
        pictureNode.clipsToBounds = true
        
        nameNode = ASTextNode()
        nameNode.attributedString = NSAttributedString(string: text, attributes: [
            NSFontAttributeName: UIFont(name: "Arial", size: 13.0)!,
            NSForegroundColorAttributeName: UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
            ])
        nameNode.alignSelf = .Center
        
        super.init()
        self.cornerRadius = 4
        self.backgroundColor = UIColor.init(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
        addSubnode(pictureNode)
        addSubnode(nameNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let spec = ASRelativeLayoutSpec(horizontalPosition: .Center, verticalPosition: .End, sizingOption: .MinimumWidth, child: nameNode)
        let layoutSpec = ASOverlayLayoutSpec(child: pictureNode, overlay: ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 5, 0),
            child: spec))
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: layoutSpec)
    }

}