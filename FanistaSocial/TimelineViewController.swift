//
//  TimelineViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/28/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import SwiftyJSON

class TimelineViewController: ASViewController, ASCollectionDelegate, ASCollectionDataSource{
    var total = 20
    var collectionNode: ASCollectionNode?
    struct State {
        var itemCount: Int
        var fetchingMore: Bool
        static let empty = State(itemCount: 20, fetchingMore: false)
    }
    
    enum Action {
        case BeginBatchFetch
        case EndBatchFetch(resultCount: Int)
    }
    
    private(set) var state: State = .empty
    var arr = [
        ImageModel(json: JSON(["url":"https://scontent-hkg3-1.cdninstagram.com/t51.2885-15/e35/13248724_1679690188961047_488336248_n.jpg", "width": 600, "height": 600])),
        ImageModel(json: JSON(["url":"https://scontent-hkg3-1.cdninstagram.com/t51.2885-15/e35/13249813_578660072309107_2014484430_n.jpg", "width": 600, "height": 600])),
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/05/17/1resize-19093881.jpg", "width": 528, "height": 669])),
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/05/17/3resize-190955049.jpg", "width": 600, "height": 600])),
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/05/17/5resize-191012802.jpg", "width": 600, "height": 600])),
        ImageModel(json: JSON(["url":"http://thumb.connect360.vn/unsafe/600x0/imgs.emdep.vn/Share/Image/2016/05/17/6resize-191020634.jpg", "width": 600, "height": 600])),
    ]
    init() {
        let flowLayout = StickyHeaderFlowLayout()
        flowLayout.headerReferenceSize = CGSizeMake(screenWidth, 52.0);
        
        let node = ASCollectionNode(collectionViewLayout: flowLayout)
        flowLayout.minimumInteritemSpacing  = 10
        flowLayout.minimumLineSpacing       = 10

        super.init(node: node)
        node.delegate = self
        node.dataSource = self
        self.collectionNode = node
        node.view.registerSupplementaryNodeOfKind(UICollectionElementKindSectionHeader)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("did load")
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("storyboards are incompatible with truth and beauty")
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        return TimelineItem(topText: "hallo", captionText: "Với những chiếc quần short cạp cao chất liệu vải lụa sẽ giúp các nàng trông sành điệu, trẻ trung hơn và item này rất hợp để mix cùng áo thun và giày bệt, set đồ tuy đơn giản nhưng không hề đơn điệu", picture: arr[indexPath.section % arr.count]);
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return total
    }
    
    func collectionView(collectionView: ASCollectionView, nodeForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let header = TimelineItemHeader(topText: "In here")
        return header
    }
    
    func collectionView(collectionView: ASCollectionView, constrainedSizeForNodeAtIndexPath indexPath: NSIndexPath) -> ASSizeRange {
        return ASSizeRangeMake(CGSize(width: screenWidth, height: 0), CGSize(width: screenWidth, height: 1000))
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView){
         
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let detailVC = TimelineDetailViewController()
        let imgModel = arr[indexPath.section % arr.count]
        detailVC.model = imgModel
        navigationController?.pushViewController(detailVC, animated: true)
    }

}

