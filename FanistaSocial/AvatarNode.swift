//
//  AvatarNode.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/28/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import AsyncDisplayKit

class AvatarNode: ASCellNode{
    
    private var avatarNode: ASNetworkImageNode
    
    init(imageURL: String){
        avatarNode = ASNetworkImageNode()
        avatarNode.contentMode = .ScaleAspectFill
        avatarNode.clipsToBounds = true
        avatarNode.URL = NSURL(string: imageURL)
        super.init()
        addSubnode(avatarNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        avatarNode.preferredFrameSize = CGSize(width: 28, height: 28)
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: avatarNode)
    }
 

}