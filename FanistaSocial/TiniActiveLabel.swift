//
//  TiniActiveLabel.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 8/23/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class TiniActiveLabel: ASTextNode{
    private var textString: NSString = ""
    private var attrString: NSMutableAttributedString
    private var callBack: ((String, TiniActiveType) -> Void)?
    override func didLoad(){
        super.didLoad()
        let tapper = UITapGestureRecognizer(target: self, action: Selector("tapRecognized:"))
        self.view.addGestureRecognizer(tapper)
    }
    
    public func setText(text: String, withHashtagColor hashtagColor: UIColor, andMentionColor mentionColor: UIColor, andCallBack callBack: (String, TiniActiveType) -> Void, font: UIFont) {
        self.callBack = callBack
        attrString = NSMutableAttributedString(string: text)
        self.attributedString = attrString
        textString = NSString(string: text)
        
        attrString.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: textString.length))
        setAttrWithName("Hashtag", wordPrefix: "#", color: hashtagColor, text: text)
        setAttrWithName("Mention", wordPrefix: "@", color: mentionColor, text: text)
    }

    func setAttrWithName(attrName: String, wordPrefix: String, color: UIColor, text: String) {
        let words = text.componentsSeparatedByString(" ")
        for word in words.filter({$0.hasPrefix(wordPrefix)}) {
            let range = textString.rangeOfString(word)
            attrString.addAttribute(NSForegroundColorAttributeName, value: color, range: range)
            attrString.addAttribute(attrName, value: 1, range: range)
            attrString.addAttribute("Clickable", value: 1, range: range)
        }
        self.attributedText = attrString
    }
    
    func tapRecognized(tapGesture: UITapGestureRecognizer) {
        // Gets the range of word at current position
        var point = tapGesture.locationInView(self.view)
        var position = self.view.closestPositionToPoint(point)
        let range = self.view.tokenizer.rangeEnclosingPosition(position, withGranularity: .Word, inDirection: 1)
        
        if range != nil {
            let location = offsetFromPosition(beginningOfDocument, toPosition: range!.start)
            let length = offsetFromPosition(range!.start, toPosition: range!.end)
            let attrRange = NSMakeRange(location, length)
            let word = attributedText.attributedSubstringFromRange(attrRange)
            
            // Checks the word's attribute, if any
            let isHashtag: AnyObject? = word.attribute("Hashtag", atIndex: 0, longestEffectiveRange: nil, inRange: NSMakeRange(0, word.length))
            let isAtMention: AnyObject? = word.attribute("Mention", atIndex: 0, longestEffectiveRange: nil, inRange: NSMakeRange(0, word.length))
            
            // Runs callback function if word is a Hashtag or Mention
            if isHashtag != nil && callBack != nil {
                callBack!(word.string, TiniActiveType.Hashtag)
            } else if isAtMention != nil && callBack != nil {
                callBack!(word.string, TiniActiveType.Mention)
            }
        }
    }

}