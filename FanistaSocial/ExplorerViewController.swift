//
//  ExplorerViewController.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/3/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import UIKit
import Koloda
import AsyncDisplayKit

private var numberOfCards: UInt = 5

class ExplorerViewController: UIViewController{
    
    @IBOutlet var kolodaView: KolodaView!
    
    private var dataSource: Array<UIImage> = {
        var array: Array<UIImage> = []
        for index in 0..<numberOfCards {
            array.append(UIImage(named: "1")!)
        }
        
        return array
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        kolodaView = KolodaView(frame: CGRectMake(10, 76, screenWidth - 20, screenHeight - 64 - (tabBarController?.tabBar.bounds.size.height)! - 32))
        kolodaView.backgroundColor = UIColor.brownColor()
        self.view.backgroundColor = UIColor.brownColor()
        self.view.addSubview(kolodaView)
        kolodaView.dataSource = self
        kolodaView.delegate = self
        self.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
    }
    
    @IBAction func leftButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.Left)
    }
    
    @IBAction func rightButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.Right)
    }
    
    @IBAction func undoButtonTapped() {
        kolodaView?.revertAction()
    }

}

extension ExplorerViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(koloda: KolodaView) {
        dataSource.insert(UIImage(named: "1")!, atIndex: kolodaView.currentCardIndex - 1)
        let position = kolodaView.currentCardIndex
        kolodaView.insertCardAtIndexRange(position...position, animated: true)
    }
    
    func koloda(koloda: KolodaView, didSelectCardAtIndex index: UInt) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://yalantis.com/")!)
    }
}
extension ExplorerViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(koloda:KolodaView) -> UInt {
        return UInt(dataSource.count)
    }
    
    func koloda(koloda: KolodaView, viewForCardAtIndex index: UInt) -> UIView {
        return UIImageView(image: dataSource[Int(index)])
    }
    /*
    func koloda(koloda: KolodaView, viewForCardOverlayAtIndex index: UInt) -> OverlayView? {
        return NSBundle.mainBundle().loadNibNamed("OverlayView",
                                                  owner: self, options: nil)[0] as? OverlayView
    }
    */
}
