//
//  TimelineItem.swift
//  FanistaSocial
//
//  Created by Nguyen Kien on 3/14/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit
import AsyncDisplayKit


let kAMMessageCellNodeAvatarImageSize: CGFloat = 34

let kAMMessageCellNodeTopTextAttributes = [NSForegroundColorAttributeName: UIColor.lightGrayColor(),
                                           NSFontAttributeName: UIFont.boldSystemFontOfSize(12)]
let kAMMessageCellNodeContentTopTextAttributes = [NSForegroundColorAttributeName: UIColor.lightGrayColor(),
                                                  NSFontAttributeName: UIFont.systemFontOfSize(12)]
let kAMMessageCellNodeBottomTextAttributes = [NSForegroundColorAttributeName: UIColor.lightGrayColor(),
                                              NSFontAttributeName: UIFont.systemFontOfSize(11)]
class TimelineItem: ASCellNode {
    private var mainPictureNode: ASNetworkImageNode!
    private var likeBadge: TimelineLikeList!
    
    private var captionNode: ASTextNode?
    private var commentButton: ASButtonNode
    private var likeButton: ASButtonNode
    private var pictureMain: ImageModel
    private var bottomSeparator: ASCellNode
    private var bottomSeparatorHair: ASCellNode
    
    init(topText: String, captionText: String?, picture: ImageModel) {
        pictureMain = picture
        likeBadge = TimelineLikeList()
        
        commentButton = ASButtonNode()
        commentButton.setImage(UIImage(named: "feed_discuss_normal"), forState: .Normal)
        commentButton.setAttributedTitle(NSAttributedString(string: "10"), forState: .Normal)
        commentButton.backgroundColor = buttonColor
        commentButton.contentEdgeInsets = UIEdgeInsetsMake(3, 12, 3, 12)
        commentButton.cornerRadius = 3
        commentButton.contentSpacing = 1
        
        likeButton = ASButtonNode()
        likeButton.setImage(UIImage(named: "feed_praise_normal"), forState: .Normal)
        likeButton.setAttributedTitle(NSAttributedString(string: "21"), forState: .Normal)
        likeButton.backgroundColor = buttonColor
        likeButton.contentEdgeInsets = UIEdgeInsetsMake(3, 12, 3, 12)
        likeButton.cornerRadius = 3
        likeButton.contentSpacing = 1
        
        bottomSeparator = ASCellNode()
        bottomSeparator.preferredFrameSize = CGSize(width: screenWidth, height: 12)
        bottomSeparator.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        
        bottomSeparatorHair = ASCellNode()
        bottomSeparatorHair.preferredFrameSize = CGSize(width: screenWidth, height: 1)
        bottomSeparatorHair.backgroundColor = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        
        super.init()
        
        captionNode = createCaption(captionText!)
        mainPictureNode = createMainPicture(imageWithURL: pictureMain.url)
        
        if let node = captionNode { addSubnode(node) }
        addSubnode(likeBadge)
        addSubnode(mainPictureNode)
        addSubnode(commentButton)
        addSubnode(likeButton)
        addSubnode(bottomSeparator)
        addSubnode(bottomSeparatorHair)
        selectionStyle = .None
        
    }
    
    func setPictureFrame(frame: CGRect){
        mainPictureNode.frame = frame
    }
    
    func createCaption(captionText: String) -> ASTextNode{
        let node = ASTextNode()
        node.layerBacked = true
        
        let captionAttribute = [NSFontAttributeName:UIFont(
            name: "Helvetica Neue",
            size: 14.0)!,
            NSForegroundColorAttributeName: textColor]
        
        
        let cText = NSAttributedString(string: captionText, attributes: captionAttribute)
        node.maximumNumberOfLines = 3
        node.attributedString = cText
        return node
    }
    
    func createMainPicture(imageWithURL URL: String) -> ASNetworkImageNode {
        let image = ASNetworkImageNode()
//        image.delegate = self
        image.URL = NSURL(string: URL)
        return image;
    }
    
    func imageRatio(imageNode: ASNetworkImageNode) -> ASRatioLayoutSpec {
//        if let image = imageNode.image {
//            let imageSize = image.size
            return ASRatioLayoutSpec(ratio: pictureMain.height / pictureMain.width, child: imageNode)
//        }
//        return ASRatioLayoutSpec(ratio: 0.5, child: imageNode)
    }
    
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var layoutCaptionSpec: ASInsetLayoutSpec = ASInsetLayoutSpec()
        if let node = captionNode{
            layoutCaptionSpec = ASInsetLayoutSpec(
                insets: UIEdgeInsetsMake(16, 10, 16, 10),
                child: node)
        }
        
        let likeSpecInset = ASInsetLayoutSpec(insets: UIEdgeInsetsMake(12, 10, 12, 10), child: ASStackLayoutSpec(direction: .Horizontal, spacing: 12, justifyContent: .Start, alignItems: .Center, children: [likeButton, commentButton]))

        let layoutSpec = ASStackLayoutSpec.verticalStackLayoutSpec()
        layoutSpec.alignItems = ASStackLayoutAlignItems.Stretch
        layoutSpec.setChildren(Array.filterNils([imageRatio(mainPictureNode), layoutCaptionSpec,likeBadge, likeSpecInset, bottomSeparator, bottomSeparatorHair]))
        
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 0, 0),
            child: layoutSpec)
    }
    
}
//extension TimelineItem: ASNetworkImageNodeDelegate {
//    
//    func imageNode(imageNode: ASNetworkImageNode, didLoadImage image: UIImage) {
//        self.setNeedsLayout()
//    }
//    
//}



