//
//  CategoryHorizontalList.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 6/7/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class CategoryHorizontalList: ASCellNode, ASCollectionDelegate, ASCollectionDataSource{
    
    var list: ASCollectionNode
    var titleNode: ASTextNode
    var arr = [
        "http://img0.ropose.com/clothes.jpeg",
        "http://img0.ropose.com/womenbags.jpeg",
        "http://img0.ropose.com/womenshoes.jpeg",
        "http://img0.ropose.com/womenaccessories.jpeg",
        "http://img0.ropose.com/jewellery.jpeg",
        "http://img0.ropose.com/lingerie.jpeg",
        "http://img0.ropose.com/beauty.jpeg",
        "http://img0.ropose.com/everything.jpeg"
    ]
    init(count: Int){
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Horizontal
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        
        list = ASCollectionNode(collectionViewLayout: flowLayout)
        list.view.showsHorizontalScrollIndicator = false
        titleNode = ASTextNode()
        titleNode.attributedString = NSAttributedString(string: "Quần áo", attributes: [
            NSFontAttributeName: UIFont(name: "Arial-BoldMT", size: 24.0)!,
            NSForegroundColorAttributeName: UIColor.init(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
            ])
        titleNode.alignSelf = .Start

        super.init()
        list.dataSource = self
        list.delegate = self
        list.preferredFrameSize = CGSize(width: screenWidth, height: 100)
        
        addSubnode(titleNode)
        addSubnode(list)
        self.setNeedsLayout()
    }
    
}

extension CategoryHorizontalList{
    func collectionView(collectionView: ASCollectionView, nodeForItemAtIndexPath indexPath: NSIndexPath) -> ASCellNode{
        let cell = CategoryItem(imageUrl: arr[indexPath.row], text: "Huy remi", sizeModel: 3.5)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: ASCollectionView, constrainedSizeForNodeAtIndexPath indexPath: NSIndexPath) -> ASSizeRange {
        return ASSizeRangeMake(CGSize(width: 0, height: 0), CGSize(width: 150, height: 150))
    }
}

extension CategoryHorizontalList{
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let layoutSpec = ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(12, 12, 12, 0),
            child: titleNode), list])
    
        return ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(0, 0, 12, 0),
            child: layoutSpec)
    }

}