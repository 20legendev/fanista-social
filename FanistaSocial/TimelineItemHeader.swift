//
//  TimelineItemHeader.swift
//  FanistaSocial
//
//  Created by Kien Nguyen on 5/31/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class TimelineItemHeader: ASCellNode {
    private let topTextNode: ASTextNode!
    private let timeTextNode: ASTextNode!
    private let avatarImageNode: ASNetworkImageNode!
    private let tmp: ASCellNode
    
    init(topText: String) {
        topTextNode = ASTextNode()
        topTextNode?.layerBacked = true
        var text = topText
        text.replaceRange(text.startIndex...text.startIndex, with: String(text[text.startIndex]).capitalizedString)
        topTextNode.attributedString = NSAttributedString(string: text, attributes: [
            NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 15.0)!,
            NSForegroundColorAttributeName: textColor
            ])

        
        avatarImageNode = ASNetworkImageNode()
        avatarImageNode.preferredFrameSize = CGSizeMake(36, 36)
        avatarImageNode.backgroundColor = UIColor.clearColor()
        avatarImageNode.URL = NSURL(string: "https://howshost.com/wp-content/uploads/2015/11/create-custom-default-avatar-50x50.jpg")
        avatarImageNode.cornerRadius = 18

        
        timeTextNode = ASTextNode()
        timeTextNode.attributedString = NSAttributedString(string: "9 phút trước", attributes: [
            NSForegroundColorAttributeName: UIColor.init(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
        ])
        timeTextNode.flexShrink = true
        
        tmp = ASCellNode()
        tmp.preferredFrameSize = CGSizeMake(screenWidth, 1)
        tmp.backgroundColor = UIColor.init(red: 204/255, green: 0/255, blue: 0/255, alpha: 0.85)
        super.init()
        
        addSubnode(tmp)
        
        if let node = topTextNode { addSubnode(node) }
        if let node = avatarImageNode { addSubnode(node) }
        addSubnode(timeTextNode)
        selectionStyle = .None
        self.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.85)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let timeSpec = ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(-1, 0, 0, 0),
            child: ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start , alignItems: .Stretch, children:[topTextNode, ASInsetLayoutSpec(
                insets: UIEdgeInsetsMake(4, 0, 0, 0),
                child: ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start , alignItems: .Stretch, children:[timeTextNode])
                )])
        )
        let spec = ASStackLayoutSpec(direction: .Horizontal, spacing: 10.0, justifyContent: .Start , alignItems: .Stretch, children: [avatarImageNode, timeSpec])
        
        let insetSpec = ASInsetLayoutSpec(
            insets: UIEdgeInsetsMake(8, 10, 8, 10),
            child: spec
        )
        return ASStackLayoutSpec(direction: .Vertical, spacing: 0, justifyContent: .Start, alignItems: .Stretch, children: [insetSpec, tmp])
    }
    
}